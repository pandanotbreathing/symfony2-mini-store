<?php
/**
 * Created by PhpStorm.
 * User: m.labinskyi
 * Date: 2/28/14
 * Time: 4:24 PM
 */ 

namespace DWD\ClientBundle\Services;

use DWD\ClientBundle\Services\ApiClient;
use Symfony\Component\Config\Definition\Exception\Exception;
use Guzzle\Http\Exception\BadResponseException;

class GoodsService
{
    /**
     * @var ApiClient
     */
    protected $client = null;

    public function __construct(ApiClient $client)
    {
        $this->client = $client;
    }

    public function getGoods()
    {
        try {
            return $this->client->getGoods();
        } catch (BadResponseException $e) {
            // throw new No Goods Exception
            return [];
        }

    }

    public function getGood($id)
    {
        try {
            return $this->client->getGood(['id' => intval($id)]);
        } catch (BadResponseException $e)
        {
            // throw new Good Not Found Exception
            return null;
        }
    }

}
