<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DWD\ClientBundle\Services;

use Guzzle\Service\Client;
use Guzzle\Service\Description\ServiceDescription;

class ApiClient extends Client
{
    const SERVICE_CONFIG_KEY = 'service';

    public function __construct($baseUrl = '', $config = null)
    {
        parent::__construct($baseUrl, $config);
        if (isset($config[self::SERVICE_CONFIG_KEY])) {
            $filePath = __DIR__ . '/../'.$config[self::SERVICE_CONFIG_KEY];
            $this->setDescription(ServiceDescription::factory($filePath));
        }
    }
}
