<?php

namespace DWD\ServerBundle\Services;

class Goods{

    private $doctrine;
    private $repositoryName = 'DWDServerBundle:Goods';

    public function __construct($doctrine){
        $this->doctirne = $doctrine;
    }


    /**
     * Settings current repository name
     * @param $name
     */
    public function setRepositoryName($name){
        $this->repositoryName = $name;
    }


    /**
     * Getting list of goods
     * @return mixed
     */
    public function getList(){
        return $this->doctirne
                    ->getRepository($this->repositoryName)
                    ->findAll();
    }


    /**
     * Getting one good
     * @param $id
     * @return mixed
     */
    public function get($id){
        return $this->doctirne
                    ->getRepository($this->repositoryName)
                    ->find($id);
    }


    /**
     * Creates good
     * @param $entity
     * @return mixed
     */
    public function create($entity){
        $manager = $this->doctirne
                        ->getManager();
        $manager->persist($entity);
        $manager->flush();

        return $entity->getId();
    }
}