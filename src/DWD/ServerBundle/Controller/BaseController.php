<?php

namespace DWD\ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class BaseController extends Controller{

    protected $response;
    protected $serializer;
    protected $entity;

    /**
     * Creating response
     * @param $code
     */
    protected function setResponse($code){
        $this->response = new Response();
        $this->response->setStatusCode($code);
        $this->response->headers->set('Content-type', 'application/json');
    }


    /**
     * Setting response body
     * @param $content
     */
    protected function setResponseContent($content){
        $this->response->setContent(json_encode($content));
    }


    /**
     * Returns current response object
     * @return mixed
     */
    protected function getResponse(){
        return $this->response;
    }


    /**
     * Returns serializer instance
     * @return Serializer
     */
    protected function getSerializer(){
        if ($this->serializer === null){
            $normalizer = new GetSetMethodNormalizer();
            $json_encoder = new JsonEncoder();
            $this->serializer = new Serializer(array($normalizer), array('json' => $json_encoder));
        }

        return $this->serializer;
    }


    /**
     * converts entity or array of entities to array
     * @param $entity
     * @return array|mixed
     */
    protected function deserializeEntity($entity){
        if (is_array($entity)){
            foreach ($entity as $key => $value){
                $entity[$key] = $this->entityToArray($value);
            }
            return $entity;
        }
        else{
            return $this->entityToArray($entity);
        }
    }


    /**
     * Converts entity to array
     * @param $entity
     * @return mixed
     */
    protected function entityToArray($entity){
        return json_decode($this->getSerializer()->serialize($entity, 'json'), true);
    }


    /**
     * Converts json to current entity
     * @param $json
     * @return object
     */
    protected function jsonToEntity($json){
        $decoded = $this->getSerializer()->decode($json, 'json');
        return $this->getSerializer()->denormalize($decoded, $this->entity);
    }
}