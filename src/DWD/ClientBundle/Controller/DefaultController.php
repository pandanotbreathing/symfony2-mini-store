<?php

namespace DWD\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\Container;
use DWD\ClientBundle\Services\GoodsService;

class DefaultController extends Controller
{
    /**
     * @var GoodsService
     */
    private $goodsService = null;

    public function __construct(Container $container, GoodsService $goodsService)
    {
        $this->container = $container;
        $this->goodsService = $goodsService;
    }

    public function indexAction()
    {
        $goods = $this->goodsService->getGoods();
        return $this->render('DWDClientBundle:Default:index.html.twig', array('goods' => $goods));
    }

    public function goodDetais($id)
    {
        $good = $this->goodsService->getGood(intval($id));
        if (null === $good) {
            throw $this->createNotFoundException('GoodID ' . $id . ' not found.');
        }
        return $this->render('DWDClientBundle:Default:good.html.twig', array('good' => $good));
    }
}
