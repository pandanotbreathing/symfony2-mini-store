<?php

namespace DWD\ClientBundle\Entity;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\AccessType;

/**
 * By default, the serializer will retrieve, or set the value via reflection,
 * but you may change this to use a public method instead:
 *
 * @AccessType("public_method")
 */
class Good
{
    /**
     * @Type("integer")
     * @var integer
     */
    protected $id;

    /**
     * @Type("string")
     * @var string
     */
    protected $name;

    /**
     * @Type("integer")
     * @var integer
     */
    protected $price = 0;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    
}
