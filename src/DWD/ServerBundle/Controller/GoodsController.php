<?php

namespace DWD\ServerBundle\Controller;

use DWD\ServerBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DWD\ServerBundle\Entity\Goods;

class GoodsController extends BaseController{

    /**
     * Current entity name;
     * @var string
     */
    protected $entity = 'DWD\ServerBundle\Entity\Goods';

    /**
     * Goods list
     * @return mixed
     */
    public function indexAction(){
        $item_list = $this->get('goods')->getList();
        $this->setResponse(Response::HTTP_OK);
        $this->setResponseContent($this->deserializeEntity($item_list));
        return $this->getResponse();
    }


    /**
     * Getting one good
     * @param $id
     * @return mixed
     */
    public function getAction($id){
        $item = $this->get('goods')->get($id);
        if (!$item){
            $this->setResponse(Response::HTTP_NOT_FOUND);
        }
        else{
            $this->setResponse(Response::HTTP_OK);
            $this->setResponseContent($this->deserializeEntity($item));
        }
        return $this->getResponse();
    }


    /**
     * Creating of good
     * @param Request $request
     * @return mixed
     */
    public function createAction(Request $request){
        $entity = $this->jsonToEntity($request->getContent());
        $validation_errors = $this->get('validator')->validate($entity);

        if (count($validation_errors) > 0){
            $this->setResponse(Response::HTTP_BAD_REQUEST);
            return $this->getResponse();
        }

        try{
            $id = $this->get('goods')->create($entity);
            $this->setResponse(Response::HTTP_CREATED);
            $response = $this->getResponse();
            $response->headers->set('Location', '/server/goods/' . $id);
            return $response;
        }
        catch(\Exception $e){
            $this->setResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->getResponse();
    }
}