<?php

namespace DWD\ServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *@ORM\Entity
 *@ORM\Table(name="goods")
 *@ORM\HasLifecycleCallbacks
 */
class Goods{

    /**
     *@ORM\Column(type="integer")
     *@ORM\Id
     *@ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     *@Assert\NotBlank()
     *@ORM\Column(type="string", length=200)
     */
    protected $name;

    /**
     *@ORM\Column(type="float")
     */
    protected $price;


    public function setName($name){
        $this->name = $name;
    }


    public function getId(){
        return $this->id;
    }


    public function getName(){
        return $this->name;
    }


    public function setPrice($price){
        $this->price = $price;
    }


    public function getPrice(){
        return $this->price;
    }
}